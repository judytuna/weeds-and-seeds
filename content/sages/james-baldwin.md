---
title: "James Baldwin"
date: 2020-08-11T16:41:00-07:00
draft: false
---

![james baldwin](/img/james-baldwin-0-top.png#top)
![james baldwin](/img/james-baldwin-2-back-green.png#bottom)
![james baldwin](/img/james-baldwin-1-mid-purple.png#mid)

> We can disagree and still love each other, unless your disagreement is rooted in my oppression and denial of my humanity and right to exist.

Happy birthday James Baldwin! Dr. Eddie Glaude’s dialogue w/ @drcornel.west & his interview on @amandaseales’s @smalldosesshow about his new text “Begin Again: James Baldwin’s America & Its Urgent Lessons for Our Own” have been so invigorating recently. Give yourself the gift of checking them out if you haven’t already 🤩⚡️🎉 [image description: “We can disagree and still love each other, unless your disagreement is rooted in my oppression and denial of my humanity and right to exist.” -James Baldwin] #jamesBaldwin #dignity #selfrespect #Baldwin