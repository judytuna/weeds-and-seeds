---
title: "Assata Shakur"
date: 2020-08-07T00:26:15-07:00
draft: false
---

# Happy birthday Assata! 

“Part of being a revolutionary is creating a vision that is more humane. That is more fun, too. That is more loving. It’s really working to create something beautiful.” -Assata Shakur 

#assata #assatashakur #revolutionary #letsgetfree #decolonize #Revolution #Decolonization #populareducation #populared #politicaleducation #consciousnessraising #liberation

Originally posted July 16, 2020 at https://www.instagram.com/p/CCuvIEIhoRd/?utm_source=ig_web_copy_link