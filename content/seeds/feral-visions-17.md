---
title: "D'Juan Owens on Embodying the Revolution"
date: 2020-07-31T00:26:15-07:00
draft: false
featured_image: '/img/feral-visions-djuan-owens.jpg'
background_color_class: "bg-blue"
---

{{< youtube iTXLHWiPrKQ >}}

D'Juan Owens and I discuss the embodiment of revolutionary strategy. What does courage feel like? Is influencer culture or celebrity culture distorting your understanding of justice work? How do we disentangle getting free from popularity contests, tokenism, trends, and respectability politics? Tune in to find out. And plz let us know in the comments what you found evocative! Thanks family.

For more support in decolonizing your mind, including online classes, check out [liberationspring.com](https://liberationspring.com/).

[Feral Visions](https://liberationspring.com/feral-visions/) is a decolonial feminist podcast brought to you by [Liberation Spring](https://liberationspring.com/). You can find Feral Visions [on SoundCloud](https://soundcloud.com/feralvisions) or download our podcasts [via iTunes](https://itunes.apple.com/us/podcast/feral-visions/id1292966148). 

To make a donation: [liberationspring.com](https://liberationspring.com/)

Follow us on SoundCloud: https://soundcloud.com/feralvisions 

Feral Visions is produced by We Rise - [www.weriseproduction.com](https://www.weriseproduction.com/)