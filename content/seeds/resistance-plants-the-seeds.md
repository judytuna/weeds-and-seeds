---
title: "Resistance Plants the Seeds"
date: 2020-08-07T00:28:11-07:00
draft: false
---

Resistance plants the seeds of a new society within the shell of the old.

Our resistance is an investment in the future- in our futures. [image description: quote reading “Resistance plants the seeds of a new society within the shell of the old” with flowers] thanks @versobooks for the illustration of this quote from Professor George Lipsitz 

#letsgetfree #decolonize #Revolution #Decolonization #populareducation #populared #politicaleducation #consciousnessraising #decolonialdiscernment #ftp #fuckthepolice #fuck12

Originally posted on June 17, 2020 at https://www.instagram.com/p/CB8VJoDhNfB/?utm_source=ig_web_copy_link