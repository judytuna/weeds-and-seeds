---
title: "Disaster Capitalism"
date: 2020-08-07T00:22:00-07:00
draft: false
featured_image: '/img/gem1.jpg'
---

![decolonial discernment gem # 1](/img/gem1.jpg)

decolonial discernment gem #1

> Most 'Diversity & Inclusion' work is a form of disaster capitalism. 
> 
> Decolonize.

Disaster capitalists bank off of tragedy. They see atrocities as so-called “market opportunities.” As opposed to ending the calamities in front of them, they ask “what can we monetize here?” How can we make fame or fortune (aka social or finance capital)? 

When our planet is a burning building, they don’t try to put out the inferno. Nope. They’re selling you aloe & aloe seeds & aloe balms… *while we’re still being incinerated. Let alone restructuring ALL the things to their advantage while our peoples are still being harmed. If you haven’t already read Professor Naomi Klein’s The Shock Doctrine: the Rise of Disaster Capitalism & watched the documentary of the same title, plz do so @ your earliest convenience to understand this vital framework. B/c, alas, there are myriad privatized products & services for sale in the ‘Diversity & Inclusion’ training space that do just this. All the while fronting as progress! 

Indeed, many entrepreneurs are banking off of this political moment in the form of ‘empowerment’ coaching & other outdated 1980s gimmicks and the like. Unsurprisingly, these market niches aren’t informed by anti-oppression praxes or liberatory knowledge + wisdom, to put it mildly lol. This weed has gotta be pulled to make space for material decolonization. Ditch that liberal multicultural distraction & come join our *anti-capitalist social movements 

🌈 PS- And yes, this is a lil’ snippet of my book “Pulling Weeds & Planting Seeds: On Decolonial Discernment” 

🎉 PPS- Make sure to cite my intellectual production if you’re inspired by these ideas & wanna share them out! Don’t steal from BIWOC- keep it ethical! 

#discernment #decolonialdiscernment #revolutionary #letsgetfree #decolonize #Revolution #Decolonization #populareducation #populared #politicaleducation #consciousnessraising #liberation #entrepreneur #anticapitalism #anticapitalist #diversity #diversityandinclusion

Originally posted at https://www.instagram.com/p/CDPLjzZBauS/?utm_source=ig_web_copy_link